package io.sokube.demo.pact.pact;

import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import au.com.dius.pact.provider.junitsupport.loader.PactFolder;
import au.com.dius.pact.provider.spring.junit5.PactVerificationSpringProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Provider("BookStore")
//@PactFolder("../pact-consumer-jvm/target/pacts")
@PactBroker(url = "http://localhost:80")
public class ContractVerificationTest {

    @MockBean
    BookRepository repo;

    @BeforeEach
    public void onsenfout() {
        MockitoAnnotations.openMocks(this);
    }
    @State("I have three well famous books")
    public void mockData() {
        //BookRepository repo = Mockito.mock(BookRepository.class);
        Mockito.when(repo.findAll()).thenReturn(Arrays.asList(new Book("F. Scott Fitzgerald", "The Great Gatsby"), new Book("Miguel de Cervantes", "Don Quixote"), new Book("William Shakespeare", "Hamlet")));
    }
    @TestTemplate
    @ExtendWith(PactVerificationSpringProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

}
