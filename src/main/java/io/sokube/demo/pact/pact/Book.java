package io.sokube.demo.pact.pact;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Date;

@Entity
public class Book {
    public String author;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public String title;

    public Book() {}
    public Book(String author, String title) {
        this.author = author;
        this.title = title;
    }
}
