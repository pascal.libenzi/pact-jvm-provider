package io.sokube.demo.pact.pact;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;

public interface BookRepository extends JpaRepository<Book, String> {
}
