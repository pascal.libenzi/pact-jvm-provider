package io.sokube.demo.pact.pact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PactProviderSbJvmApplication {

    public static void main(String[] args) {
        SpringApplication.run(PactProviderSbJvmApplication.class, args);
    }

}
